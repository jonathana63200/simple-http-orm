<?php

namespace App\Orm;

use App\Orm\Provider\RestProvider;
use DateTime;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EntityHelper
{
    public const CREATION_CONTEXT  = 0;
    public const NORMALIZE_CONTEXT = 1;


    public string $ressourceId;
    public bool   $loaded =false;

    protected function instanciateDependencies(string $ressourceId, string $object): object
    {
        $object = new $object();
        $id = explode($object::API_RESSOURCE. '/', $ressourceId)[1];
        $object->setId($id);
        $object->setRessourceId($ressourceId);
        $object->setLoaded(false);

        return $object;
    }

    public function load(object $class)
    {
        $apiRessources = $class::API_RESSOURCE;

        $provider = new RestProvider(new CurlHttpClient());
        $resp = $provider->getRequest($class->getRessourceId());

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders             = [new JsonEncoder()];
        $normalizer           = new ObjectNormalizer($classMetadataFactory);

        $obj = (new Serializer([$normalizer], $encoders))->deserialize($resp, $class::class, 'json', [
            AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => false,
        ]);

        $obj->setloaded(true);
        return $obj;
    }
    public function loadExistingObject(object $object): object
    {
        $ressourceName = $object::API_RESSOURCE;


        $provider = new RestProvider(new CurlHttpClient());

        $resp = $provider->getRequest($object->getRessourceId());

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new JsonEncoder()];
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer], $encoders);

        $obj = $serializer->deserialize($resp, $object::class, 'json', [
            AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => false,
            AbstractNormalizer::OBJECT_TO_POPULATE => $object
        ]);
        $object->setLoaded(true);
        return $object;
    }

    protected function setSimpleDependency(string $ressourceId, string $object = null): object|string
    {
        if (isset($this->context) &&  $this->context === 0) {
            return $ressourceId;
        }

        if (null !== $object) {
            if (class_exists($object)) {
                return $this->instanciateDependencies($ressourceId, $object);
            }
        }

        return $ressourceId;
    }
    protected function getSimpleDependency(mixed $attribute): object|string
    {
        $type = gettype($attribute);
        if (('object' === $type) && false === $attribute->loaded) {
            $this->loadExistingObject($attribute);
            return $attribute;
        }

        return $attribute;
    }

    protected function setCollectionDependency(string|object $ressourceId, string $object = null): object|string
    {
        if (gettype($ressourceId) === 'object') {
            return $ressourceId;
        }
        if ((null !== $object) && class_exists($object)) {
            return $this->instanciateDependencies($ressourceId, $object);
        }

        return $ressourceId;
    }
    protected function getCollectionDependencies(mixed $objects, bool $withLoading = false)
    {
        if ($withLoading) {
            $this->loadAllDependencies($objects);
            return $objects;
        }

        return $objects;
    }

    ##########################################################################
    ## TOOLS
    ##########################################################################
    protected function getDateTimeFromString(string $date): \DateTimeInterface
    {
        $str = strtotime($date);

        $date = new DateTime();
        $date->setTimestamp($str);

        return $date;
    }
    protected function loadAllDependencies(ArrayCollection $objects): void
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new JsonEncoder()];
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer], $encoders);
        $provider = new RestProvider(new CurlHttpClient());

        $ressourceName = $objects->first()::API_RESSOURCE;

        $resp = [];
        foreach ($objects as $object) {
            $resp[] = $provider->asyncRequest($ressourceName, $object->getRessourceId());
        }
        $reponses = $provider->fetch($resp);

        $respArray = [];
        foreach ($reponses as $response) {
            $decoded = json_decode($response, true);
            $respArray[$decoded['id']] = $decoded;
        }

        $populatedObj = [];
        foreach ($objects as $object) {
            $oReflectionClass = new \ReflectionClass($object);
            $ns = $oReflectionClass->getName();
            $id = $object->getId();
            $newObj =  $normalizer->denormalize($respArray[$id], $ns, null, [
                AbstractNormalizer::OBJECT_TO_POPULATE => $object
            ]);
            $newObj->loaded = true;
            $populatedObj[] = $newObj;
        }
    }

    ##########################################################################
    ## DEFAULT ENTITY PARAMETERS
    ##########################################################################
    public function isLoaded(): bool
    {
        return $this->loaded;
    }
    public function setLoaded(bool $loaded): void
    {
        $this->loaded = $loaded;
    }
    public function getRessourceId(): string
    {
        return $this->ressourceId;
    }
    public function setRessourceId(string $ressourceId): void
    {
        $this->ressourceId = $ressourceId;
    }

    public function create(object $object = null): object
    {
        if (null === $object) {
            $object = $this;
        }

        $apiRessources = $object::API_RESSOURCE;

        $provider = new RestProvider(new CurlHttpClient());

        $uri = $uri = '/api/' . $apiRessources;

        $dateCallback = function ($innerObject) {
            return $innerObject instanceof \DateTime ? $innerObject->format('Y-m-d H:i:s') : '';
        };

        $defaultContext = [
            AbstractNormalizer::CALLBACKS => [
                'created' => $dateCallback,
                'updated' => $dateCallback
            ],
        ];

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer(defaultContext: $defaultContext )];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($object, 'json');

        $resp = $provider->postRequest($uri, $jsonContent);

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders   = [new JsonEncoder()];
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer], $encoders);

        $obj = $serializer->deserialize($resp, $object::class, 'json', [
            AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => false,
            AbstractNormalizer::OBJECT_TO_POPULATE => $object
        ]);
        $object->setLoaded(true);

        return $object;
    }

}