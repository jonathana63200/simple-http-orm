<?php

namespace App\Orm;

use App\Orm\Provider\RestProvider;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\Common\Annotations\AnnotationReader;

class EntityManager extends EntityHelper
{
    public function __construct(
        private readonly RestProvider $restProvider,
    ) {
    }

    public function findOneById(string $class, mixed $ressourceId): object
    {
        $apiRessources = $class::API_RESSOURCE;

        if (is_int($ressourceId)) {
            $uri = '/api/' . $apiRessources . '/' . $ressourceId;
        } else {
            $uri = $ressourceId;
        }

        $resp = $this->restProvider->getRequest($uri);

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new JsonEncoder()];
        $normalizer = new ObjectNormalizer($classMetadataFactory);

        $obj = (new Serializer([$normalizer], $encoders))->deserialize($resp, $class, 'json', [
            AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => [
                $class => [
                    'context' => 1
                ]
            ],
            AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => false,
        ]);

        $obj->loaded = true;
        $obj->setRessourceId($uri);
        return $obj;
    }
    public function find(string $class, ?array $searchfilters = null, ?array $orderFilters = null): ?ArrayCollection
    {
        $apiRessource = $class::API_RESSOURCE;

        $uri =  '/api/' . $apiRessource;
        if (null !== $searchfilters || null !== $orderFilters) {
            $strRequest = $this->buildRequest($apiRessource, $searchfilters, $orderFilters);
            $uri = '/api/' . $strRequest;
        }

        $resp = $this->restProvider->getRequest($uri);

        $resp = json_decode($resp, true);

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new JsonEncoder()];
        $normalizer = new ObjectNormalizer($classMetadataFactory);

        $collection = new ArrayCollection();
        foreach ($resp as $value) {
            $newObject = $normalizer->denormalize($value, $class, null, []);
            $newObject->setLoaded(true);
            $collection->add($newObject);
        }

        return $collection;
    }

    private function buildRequest(string $apiRessource, ?array $searchFilters, ?array $orderrFilters): string
    {
        // CONSTRUCT REQUEST
        $strRequest = $apiRessource .'?';

        if (null !== $searchFilters) {
            foreach ($searchFilters as $key => $searchFilter) {
                if (is_array($searchFilter)) {
                    foreach ($searchFilter as $value) {
                        $strRequest .= $key . "[]=" . $value . '&';
                    }
                } else {
                    $strRequest .= $key . "=" . $searchFilter;
                }
            }
        }

        if (null !== $orderrFilters) {
            foreach ($orderrFilters as $key => $value) {
                $strRequest .= 'order' . "[" . $key ."] =" .strtolower($value) ."&";
            }
        }

        return substr($strRequest, 0, -1);
    }
}
