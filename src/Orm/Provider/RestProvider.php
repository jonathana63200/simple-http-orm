<?php

namespace App\Orm\Provider;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RestProvider
{
    public const HOST_AND_BASE = "http://10.6.0.5";

    public function __construct(
        private readonly HttpClientInterface $httpClient,
    )
    {
        $this->responses = new ArrayCollection();
    }

    public function getRequest(string $uri): string
    {
        $httpClient = $this->httpClient;
        $url = self::HOST_AND_BASE . $uri;
        $res = $httpClient->request('GET', $url, [
            'headers' => [
                'accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Cache-Control' => "no-cache ",
            ],
        ]);
        return $res->getContent();
    }
    public function postRequest(string $uri, string $content): string
    {
        $httpClient = $this->httpClient;
        $url = self::HOST_AND_BASE . $uri;

        $res = $httpClient->request('POST', $url, [
            'headers' => [
                'accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Cache-Control' => "no-cache ",
            ],
            'body' => $content
        ]);

        return $res->getContent();
    }
    public function asyncRequest(string $ressource, string $uri): \Symfony\Contracts\HttpClient\ResponseInterface
    {
        $httpClient = $this->httpClient;

        $url = self::HOST_AND_BASE . $uri;

        $res = $httpClient->request('GET', $url, [
            'headers' => [
                'accept' => 'application/json',
            ],
        ]);

        return $res;
    }
    public function fetch(array $responses)
    {
        $content = [];
        foreach ($this->httpClient->stream($responses) as $response => $chunk) {
            if ($chunk->isFirst()) {
                // headers of $response just arrived
                // $response->getHeaders() is now a non-blocking call
            } elseif ($chunk->isLast()) {
                // the full content of $response just completed
                $content[] = $response->getContent();
            } else {
                // $chunk->getContent() will return a piece
                // of the response body that just arrived
            }
        }

        return $content;
    }
}
